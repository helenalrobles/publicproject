/* Copyright 2016, Romina Alfaro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _UART_H_
#define _UART_H_

/*==================[inclusions]=============================================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** initialize USART0 using ring buffers
 *
 * @param baudrate desired baud rate
 */
void uart0Init(uint32_t baudrate);


/** initialize USART1 using ring buffers
 *
 * @param baudrate desired baud rate
 */
void uart1Init(uint32_t baudrate);


/** initialize USART2 using ring buffers
 *
 * @param baudrate desired baud rate
 */
void uart2Init(uint32_t baudrate);



/** initialize USART3 using ring buffers
 *
 * @param baudrate desired baud rate
 */
void uart3Init(uint32_t baudrate);



/** send data through USART0
 *
 * @param data pointer to data
 * @param len data length in bytes
 * @return number of bytes placed in tx buffer
 */
uint32_t uart0send(void * data, size_t len);


/** send data through USART1
 *
 * @param data pointer to data
 * @param len data length in bytes
 * @return number of bytes placed in tx buffer
 */
uint32_t uart1send(void * data, size_t len);


/** send data through USART2
 *
 * @param data pointer to data
 * @param len data length in bytes
 * @return number of bytes placed in tx buffer
 */
uint32_t uart2send(void * data, size_t len);


/** send data through USART3
 *
 * @param data pointer to data
 * @param len data length in bytes
 * @return number of bytes placed in tx buffer
 */
uint32_t uart3send(void * data, size_t len);


/** receive data from USART0
 *
 * @param data buffer to store data
 * @param len buffer size
 * @return number of bytes readed from rx buffer
 */
uint32_t uart0recv(void * data, size_t len);


/** receive data from USART1
 *
 * @param data buffer to store data
 * @param len buffer size
 * @return number of bytes readed from rx buffer
 */
uint32_t uart1recv(void * data, size_t len);


/** receive data from USART2
 *
 * @param data buffer to store data
 * @param len buffer size
 * @return number of bytes readed from rx buffer
 */
uint32_t uart2recv(void * data, size_t len);


/** receive data from USART3
 *
 * @param data buffer to store data
 * @param len buffer size
 * @return number of bytes readed from rx buffer
 */
uint32_t uart3recv(void * data, size_t len);




/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* #ifndef _UART_H_ */
