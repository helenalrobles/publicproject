/* Copyright 2016, Pablo Ridolfi
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "uart.h"
#include "portmacro.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "projdefs.h"

//#include "semphr.h"


/*==================[macros and definitions]=================================*/

/** uart buffers length */
#define UART_BUFFER_LEN 64

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/** Ringbuffers for UART operation */
static RINGBUFF_T txrb;
static RINGBUFF_T rxrb;

/** buffers for ringbuffer */
static uint8_t txbuff[UART_BUFFER_LEN];
static uint8_t rxbuff[UART_BUFFER_LEN];

/*==================[external data definition]===============================*/
//xSemaphoreHandle sem_UART1;
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

//void UART0_IRQHandler(void)
////void UART3_IRQHandler(void)
//{
////	portBASE_TYPE ccontexto;
////	xSemaphoreGiveFromISR(sem_UART0, &ccontexto);
//	Chip_UART_IRQRBHandler(LPC_UART0, &rxrb, &txrb);
////	portEND_SWITCHING_ISR(ccontexto);
//	//Chip_UART_IRQRBHandler(LPC_UART3, &rxrb, &txrb);
//}

//void UART1_IRQHandler(void)
////void UART3_IRQHandler(void)
//{
////	portBASE_TYPE ccontexto;
//	xSemaphoreGiveFromISR(sem_UART1);
//	Chip_UART_IRQRBHandler(LPC_UART1, &rxrb, &txrb);
////	portEND_SWITCHING_ISR(ccontexto);
//	//Chip_UART_IRQRBHandler(LPC_UART3, &rxrb, &txrb);
//}


void uart0Init(uint32_t baudrate)
/* Inicializacion de P0.2 y P0.3 para RXD y TXD respectivamente, modulo WiFi*/
{
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 2, IOCON_MODE_INACT | IOCON_FUNC1);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 3, IOCON_MODE_INACT | IOCON_FUNC1);

	Chip_UART_Init(LPC_UART0);
	Chip_UART_TXEnable(LPC_UART0);
	Chip_UART_SetBaud(LPC_UART0, baudrate);

/* el tercer argumento es el item size. si se cambia cambia la cant de bytes a enviar*/
	RingBuffer_Init(&txrb, txbuff, 1, UART_BUFFER_LEN);
	RingBuffer_Init(&rxrb, rxbuff, 1, UART_BUFFER_LEN);

	Chip_UART_IntEnable(LPC_UART0, UART_IER_RBRINT);
	Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);

	NVIC_EnableIRQ(UART0_IRQn);
}

void uart1Init(uint32_t baudrate)
/* Inicializacion de P0.15 y P0.16 para RXD y TXD respectivamente, modulo GPRs*/
{
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 15, IOCON_MODE_INACT | IOCON_FUNC1);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 16, IOCON_MODE_INACT | IOCON_FUNC1);


	Chip_UART_Init(LPC_UART1);
	Chip_UART_TXEnable(LPC_UART1);
	Chip_UART_SetBaud(LPC_UART1, baudrate);


	/* el tercer argumento es el item size. si se cambia cambia la cant de bytes a enviar*/
	RingBuffer_Init(&txrb, txbuff, 1, UART_BUFFER_LEN);
	RingBuffer_Init(&rxrb, rxbuff, 1, UART_BUFFER_LEN);

	Chip_UART_IntEnable(LPC_UART1, UART_IER_RBRINT);
	Chip_UART_IntDisable(LPC_UART1, UART_IER_THREINT);


	NVIC_EnableIRQ(UART1_IRQn);
}


void uart2Init(uint32_t baudrate)

{
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 10, IOCON_MODE_INACT | IOCON_FUNC1);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 11, IOCON_MODE_INACT | IOCON_FUNC1);


	Chip_UART_Init(LPC_UART2);
	Chip_UART_TXEnable(LPC_UART2);
	Chip_UART_SetBaud(LPC_UART2, baudrate);


	// el tercer argumento es el item size. si se cambia cambia la cant de bytes a enviar?
	RingBuffer_Init(&txrb, txbuff, 1, UART_BUFFER_LEN);
	RingBuffer_Init(&rxrb, rxbuff, 1, UART_BUFFER_LEN);

	Chip_UART_IntEnable(LPC_UART2, UART_IER_RBRINT);
	Chip_UART_IntDisable(LPC_UART2, UART_IER_THREINT);


	NVIC_EnableIRQ(UART2_IRQn);
}

void uart3Init(uint32_t baudrate)

{
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 0, IOCON_MODE_INACT | IOCON_FUNC2);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 1, IOCON_MODE_INACT | IOCON_FUNC2);


	Chip_UART_Init(LPC_UART3);
	Chip_UART_TXEnable(LPC_UART3);
	Chip_UART_SetBaud(LPC_UART3, baudrate);


	// el tercer argumento es el item size. si se cambia cambia la cant de bytes a enviar?
	RingBuffer_Init(&txrb, txbuff, 1, UART_BUFFER_LEN);
	RingBuffer_Init(&rxrb, rxbuff, 1, UART_BUFFER_LEN);

	Chip_UART_IntEnable(LPC_UART3, UART_IER_RBRINT);
	Chip_UART_IntDisable(LPC_UART3, UART_IER_THREINT);


	NVIC_EnableIRQ(UART3_IRQn);
}



uint32_t uart0send(void * data, size_t len)
{
	return Chip_UART_SendRB(LPC_UART0, &txrb, data, len);
}

uint32_t uart0recv(void * data, size_t len)
{
	return Chip_UART_ReadRB(LPC_UART0, &rxrb, data, len);
}



uint32_t uart1send(void * data, size_t len)
{
	return Chip_UART_SendRB(LPC_UART1, &txrb, data, len);
}

uint32_t uart1recv(void * data, size_t len)
{
	return Chip_UART_ReadRB(LPC_UART1, &rxrb, data, len);
}



uint32_t uart2send(void * data, size_t len)
{
	return Chip_UART_SendRB(LPC_UART2, &txrb, data, len);
}

uint32_t uart2recv(void * data, size_t len)
{
	return Chip_UART_ReadRB(LPC_UART2, &rxrb, data, len);
}



uint32_t uart3send(void * data, size_t len)
{
	return Chip_UART_SendRB(LPC_UART3, &txrb, data, len);
}

uint32_t uart3recv(void * data, size_t len)
{
	return Chip_UART_ReadRB(LPC_UART3, &rxrb, data, len);
}

void UART0_IRQHandler(void)
{
	static portBASE_TYPE xHigherPriorityTaskWoken;
/* pxHigherPriorityTaskWoken
 * It is possible that a single semaphore will have one or more tasks
blocked on it waiting for the semaphore to become available. Calling
xSemaphoreGiveFromISR() can make the semaphore available, and
so cause such a task to leave the Blocked state. If calling
xSemaphoreGiveFromISR() causes a task to leave the Blocked state,
and the unblocked task has a priority higher than the currently
executing task (the task that was interrupted), then
xSemaphoreGiveFromISR() will internally set
*pxHigherPriorityTaskWoken to pdTRUE.
If xSemaphoreGiveFromISR() sets this value to pdTRUE then a
context switch should be performed before the interrupt is exited. This
will ensure the interrupt returns directly to the highest priority Ready
state task.

Returned value

There are two possible return values:
3. pdPASS
pdPASS will only be returned if the call to
xSemaphoreGiveFromISR() was successful.
4. pdFAIL
If a semaphore is already available, it cannot be given, and
xSemaphoreGiveFromISR() will return pdFAIL.
 */


//	xHigherPriorityTaskWoken = pdFALSE;
//	xSemaphoreGiveFromISR(sem_UART0, &xHigherPriorityTaskWoken);
	Chip_UART_IRQRBHandler(LPC_UART0, &rxrb, &txrb);
}


void UART1_IRQHandler(void)
{
	static portBASE_TYPE xHigherPriorityTaskWoken;
/* pxHigherPriorityTaskWoken
 * It is possible that a single semaphore will have one or more tasks
blocked on it waiting for the semaphore to become available. Calling
xSemaphoreGiveFromISR() can make the semaphore available, and
so cause such a task to leave the Blocked state. If calling
xSemaphoreGiveFromISR() causes a task to leave the Blocked state,
and the unblocked task has a priority higher than the currently
executing task (the task that was interrupted), then
xSemaphoreGiveFromISR() will internally set
*pxHigherPriorityTaskWoken to pdTRUE.
If xSemaphoreGiveFromISR() sets this value to pdTRUE then a
context switch should be performed before the interrupt is exited. This
will ensure the interrupt returns directly to the highest priority Ready
state task.

Returned value

There are two possible return values:
3. pdPASS
pdPASS will only be returned if the call to
xSemaphoreGiveFromISR() was successful.
4. pdFAIL
If a semaphore is already available, it cannot be given, and
xSemaphoreGiveFromISR() will return pdFAIL.
 */


	//xHigherPriorityTaskWoken = pdFALSE;
	//xSemaphoreGiveFromISR(sem_UART1, &xHigherPriorityTaskWoken);
	Chip_UART_IRQRBHandler(LPC_UART1, &rxrb, &txrb);
}

/*==================[end of file]============================================*/

