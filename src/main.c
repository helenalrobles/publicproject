/* Copyright 2016, Romina Alfaro
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/**
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"
#include "board.h"
#include "uart.h"
#include "stdio.h"
#include "string.h"


//FreeRTOS inclusions
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "FreeRTOSConfig.h"

//#include "wifi.h" no aparece el archivo --> armarlo
#include "lcd.h"
#include "keypad.h"

/*==================[macros and definitions]=================================*/
/* definicion de  colas*/

xQueueHandle qUART0;
xQueueHandle qUART1;
xQueueHandle qLCD;
xQueueHandle qCodigo;
xQueueHandle qADC0;
xQueueHandle qADC1;
xQueueHandle qPassword;

#define TAM_PILA (4*configMINIMAL_STACK_SIZE)

/** uart buffers length */
#define UART_BUFFER_LEN 64


//DEFINE'S ADC
#define RATE_ADC	200
#define TEMP_MAX    (124)*(40) /*ADC de 12 bits, proporción: aprox 124 c/ 1°C*/
#define VdL_MIN      1000 //Ver ajustando el pote
#define VdL_MAX      3000 //Ver ajustando el pote

//PINOUT
//SENSOR INFRARROJO
#define SINFRA_PORT	0
#define SINFRA_PIN	22
//SENSOR PIR
#define SPIR_PORT	0
#define SPIR_PIN	21
//SENSOR DE GAS
#define SGAS_PORT	0
#define SGAS_PIN	27
//RELE
#define RELE1_PORT	0
#define RELE1_PIN	28
#define RELE2_PORT	2
#define RELE2_PIN	13


/** Ringbuffers for UART operation */
//static RINGBUFF_T txrb;
//static RINGBUFF_T rxrb;

///** buffers for ringbuffer */
//static uint8_t txbuff[UART_BUFFER_LEN];
//static uint8_t rxbuff[UART_BUFFER_LEN];

/* Tamaño del buffer de ingreso de contraseña */
#define TAM_PASSW 5
char password[]="1234";

//xSemaphoreHandle semb_Cod; // habilita la escritura del codigo para desactivar alarma
xSemaphoreHandle semb_UART0; // habilita el envio de datos al log or uart0
xSemaphoreHandle semb_UART1; // habilita el aviso al celular por uart1
xSemaphoreHandle semb_AlyLu; // habilita el rele para activar la carga que controla luces y sirena
xSemaphoreHandle semb_Linea; // habilita el rele para activar la interrupcion de la linea
xSemaphoreHandle semb_Pass_OK;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);
static void Periph_Init(void);
//static void Init_ESP(void);
static void ADC0_2_init(void) {
/*	modo 2: sin pullup ni pulldown, funcion 1: ADC */
/* nicializacion Entrada Analogica 2 Pin17 P0.23 AD0.2  Sensor de Tension de linea  (S5) */
	Chip_IOCON_PinMux(LPC_IOCON, 0, 23, 2, 1);
}
static void ADC0_1_init(void) {
/*	modo 2: sin pullup ni pulldown, funcion 1: ADC */
	//Inicializacion Entrada Analogica 1 Pin16 P0.24 AD0.1
	Chip_IOCON_PinMux(LPC_IOCON, 0, 24, 2, 1);//revisar
}

//static void Init_ESP(void){
//	int i;
//	//envio datos de la red Wifi: SSID + Psswd
//	Chip_UART_SendBlocking(LPC_UART0, (char *)"AT+CWJAP=\"AndroidAP\",\"123456789\"\r\n", 34);
//	for(i=0; i<0xFFFF; i++); //retardo por software para que el modulo ESP8266 pueda responder. Hay que hacerlo con cada envio de comandos
//	//seteo el modulo para multiples conexiones entrantes y salientes
//	Chip_UART_SendBlocking(LPC_UART0, (char *)"AT+CIPMUX=1\r\n", 13);
//	for(i=0; i<0xFFFF; i++);
//	//seteo el modulo como servidor y habilito el puerto 80 (es necesario por haber configurado las multiples conexiones)
//	Chip_UART_SendBlocking(LPC_UART0, (char *)"AT+CIPSERVER=1,80\r\n", 19);
//	for(i=0; i<0xFFFF; i++);
//}

/* Inicializa ADC CH 1 y 2 */
void adcInit(void)
{
    ADC_CLOCK_SETUP_T adc;

    Chip_ADC_Init(LPC_ADC, &adc);
    Chip_ADC_SetSampleRate(LPC_ADC, &adc, RATE_ADC);
    Chip_ADC_EnableChannel(LPC_ADC, ADC_CH1, ENABLE);
    Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH1, ENABLE);
    Chip_ADC_EnableChannel(LPC_ADC, ADC_CH2, ENABLE);
    Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH2, ENABLE);
    Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);
    NVIC_EnableIRQ(ADC_IRQn);
}

static void initHardware(void) {
	SystemCoreClockUpdate();
	Board_Init();
	Periph_Init();
//	Init_ESP(); //inicializo el modulo ESP8266 con los datos de la red wifi a la que se conecta
	//LCD_Init();
}

static void Periph_Init(void) {
/* Inicializacion UART1 Pin_Tx1 13 P0.15, pin_Rx1 14 P0.16 manejo de GRPS*/
		uart1Init(9600);
/* Inicializacion Entrada Analogica 1 Pin16 P0.24 AD0.1 y P0.25  pin 17 Sensor de Temperatura (S2) */
/* usaremos el lm35 con salida analogica */
		//ADC0_1_init();
/* Inicializacion UART0 Pin_Tx0 21 P0.2, pin_Rx0 22 P0.3 manejo de WiFi */
//		uart0Init(115200);
/* Inicializacion Entrada Digital 1 Pin23 P0.21 Sensor PIR (S3)*/
		Chip_GPIO_WriteDirBit(LPC_GPIO, SPIR_PORT, SPIR_PIN, 0);
/* Inicializacion Entrada Digital 2 Pin24 P0.22  Sensor Infrarojo (S4)*/
		Chip_GPIO_WriteDirBit(LPC_GPIO, SINFRA_PORT, SINFRA_PIN, 0);
/* Inicializacion Entrada Digital 0 Pin25 P0.27 Sensor de Gas (S1)*/
		Chip_GPIO_WriteDirBit(LPC_GPIO, SGAS_PORT, SGAS_PIN, 0);
/* Inicializacion Salida Digital 0 Pinx P0.28 Manejo Rele 1 para luces y alarma*/
		Chip_GPIO_WriteDirBit(LPC_GPIO, RELE1_PORT, RELE1_PIN, 1);
		Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE1_PORT,RELE1_PIN);

/* Inicializacion Salida Digital 1 Pinx P2.13 Manejo Rele 2 para tension de linea */
		Chip_GPIO_WriteDirBit(LPC_GPIO, RELE2_PORT, RELE2_PIN, 1);
/* Inicializacion Entrada Analogica 2 Pin17 P0.23 AD0.2  Sensor de Tension de linea  (S5): usaremos el lm35 con salida analogica */
        //ADC0_2_init();

		/*
		 *
		 * DESCOMENTAR LA LÍNEA ARREGLANDO SU CÓDIGO
		 */
        //adcInit();
}

/* =========================Referencias Web====================================*/
/*	http://web.ist.utl.pt/~ist11993/FRTOS-API/group___semaphores.html*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

//Desde la interrupción del ADC evalúo los valores del las entradas analógicas
/*
void ADC_IRQHandler(void)
{
    static uint16_t data1, data2;
    //Data 1 es de channel 1 - V de temperatura
    //Data 2 es de channel 2 - V de linea

    Chip_ADC_ReadValue(LPC_ADC, ADC_CH1, &data1);

    Chip_ADC_ReadValue(LPC_ADC, ADC_CH2, &data2);
    //Si alguna de las dos está por fuera de lo valores que debe tomar:
    //más de 40°C Temperatura o +-10% de 220V tensión de línea
    if((data1 > TEMP_MAX) || (data2 < VdL_MIN) || (data2 > VdL_MAX) ) {
        xSemaphoreGive(sem_UART1); //habilita llamado
        xSemaphoreGive(sem_UART0); // envia log
        xSemaphoreGive(sem_AlyLu); // habilita circuito de luz y alarma
    }
}
*/
/**	@brief: Tarea para la depuración de errores en el código
 * 	@return none */
void debug (void *pvParameters)
{
	int count = 1;

	vTaskDelay(100);
	LCD_Init();
	LCD_GoToxy(0, 0);
	LCD_Print("Hola!");
	while(1)
	{
		/*
        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE2_PORT,RELE2_PIN);
        Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE2_PORT,RELE2_PIN);
        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE2_PORT,RELE2_PIN);
        Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE2_PORT,RELE2_PIN);
        //Chip_GPIO_SetPinToggle(LPC_GPIO, RELE2_PORT, RELE2_PIN);// Togglea el rele1
		Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE2_PORT,RELE2_PIN);
        //Chip_GPIO_SetPinToggle(LPC_GPIO, RELE2_PORT, RELE2_PIN);// Togglea el rele1

        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        //Chip_GPIO_SetPinToggle(LPC_GPIO, RELE1_PORT, RELE1_PIN);// Togglea el rele1
		Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        //Chip_GPIO_SetPinToggle(LPC_GPIO, RELE1_PORT, RELE1_PIN);// Togglea el rele1
         */
		LCD_GoToxy(0, count);
		LCD_Print("Prueba");
		count++;
		count%=4;
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}

/**	@brief : Lectura de los sensores: Infrarojo, PIR, GAS. Si los sensores estan activados se habilitan los semaforos de las tareas
 * correspondientes.
  	@return none */
static void read_sensores (void *ss)
{
	while(1)
	{
		/* INFRAROJO */
		/*if (Chip_GPIO_GetPinState(LPC_GPIO, SINFRA_PORT, SINFRA_PIN)==true)	//Revisar si es activo alto o bajo
		{
			xSemaphoreTake(sem_Pass_OK,0);
			xSemaphoreGive(sem_UART1); //habilita llamado
			xSemaphoreGive(sem_UART0); // envia log
			xSemaphoreGive(sem_AlyLu); // habilita circuito de luz y alarma
		}*/
		/* GAS */
		if(Chip_GPIO_GetPinState(LPC_GPIO, SGAS_PORT, SGAS_PIN)==false)
		{
			xSemaphoreTake(semb_Pass_OK,0);
			xSemaphoreGive(semb_UART1); //habilita llamado
			xSemaphoreGive(semb_UART0); // envia log
			xSemaphoreGive(semb_AlyLu); // habilita circuito de luz y alarma
		}
		/* PIR */
		/*
		if (Chip_GPIO_GetPinState(LPC_GPIO, SPIR_PORT, SPIR_PIN)==true)
		{
			xSemaphoreTake(sem_Pass_OK,0);
			xSemaphoreGive(sem_UART1); //habilita llamado
			xSemaphoreGive(sem_UART1); //habilita llamado
			xSemaphoreGive(sem_UART0); // envia log
			xSemaphoreGive(sem_AlyLu); // habilita circuito de luz y alarma
			//Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE2_PORT,RELE2_PIN);
		}
			//else
	        //Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE2_PORT,RELE2_PIN);
	    */
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}

/**	@brief : Lectura sensor de temperatura: El ADC dejara las conversiones en la cola, luego la tarea realizara la conversion lineal con una multiplicacion y offset pra luego dejarlo en el vector
general de estados. Si se supera un umbral establecido se genera el llamado y el log
el rango del sensor es de -55°C a 150°C, se considera que la temperatura debera estar por debajo de los 40° para que no se generen avisos*/
 /* @return none */
/*
static void read_EA1 (void* d)
{
	while (1)
	{
		// Extraccion de dato de la cola conversion del valor
		// verificacion del valor. T_max=40°C, T_amb es el valor obtenido

//			if (T_amb>=40)
//			{
//				xSemaphoreGive(sem_UART0); // envia log
//				vTaskDelay(50 / portTICK_RATE_MS);
//			}
		}


}*/

/**	@brief : Lectura del sensado de linea v_linea es la variable donde esta guardado el valor actual de la tension de linea,
 * U_inf es el umbral definido como piso de tension tolerable, U_sup es la maxima tension de linea tolerable
El ADC dejara las conversiones en la cola, luego la tarea analizara los datos (para entender si esta dentro del umbral o no)
 si corresponde generara un give para los semaforos de wifi, gprs y apertura/corte de rele
 	@return none */
/*
static void read_EA2 (void* e)
{
//	int U_inf = 209;
//	int U_sup = 231;
	// toma los datos de la cola
	while (1)
	{
//		if ((V_linea <= U_inf>)(V_linea >= U_sup))
//		{
//			xSemaphoreGive(sem_UART0); // envia log
//			vTaskDelay(50 / portTICK_RATE_MS);
//			xSemaphoreGive(sem_Linea);// habilita el rele de corte
//
//		}
	}

}
*/
/**	@brief: Activacion del rele de corte de tension cuando se activa un alta o baja de tension
 * 	@return none */
/*
static void v_linea (void* f){

	while (1){

			xSemaphoreTake(sem_Linea,portMAX_DELAY);
//			xSemaphoreGive(sem_UART0); // envia log
			vTaskDelay(50 / portTICK_RATE_MS);
			//Chip_GPIO_SetPinState(LPC_GPIO, RELE2_PORT, RELE2_PIN, 1);// habilita el rele que esta NC y queda NA
			xSemaphoreGive(sem_Linea,portMAX_DELAY);
	}

}
*/
/**	@brief : Realiza el llamado cuando se activa el sensor IR,PIR o GAS
 * 	@return none */

static void send_SMS (void* g)
{
	while (1)
	{
		if (xSemaphoreTake(semb_UART1,(portMAX_DELAY)) == pdTRUE) //portMAX_DELAY espera indefinidamente al semafro
		{
			xSemaphoreTake(semb_UART1,(portMAX_DELAY));
			Chip_UART_SendBlocking(LPC_UART1, (uint8_t *)"ATD02234214306;\r\n", 19);
			vTaskDelay(50 / portTICK_RATE_MS);
		}
	}
}

/**	@brief : Activa/Apaga  el circuito de encendido de luz y alarma (tooglea rele).esta salida se activa hacia el rele solo
 * si los sensores de presencia (pir o infrarrojo) detectan presencia y desactiva luces y alarma que estan en un unico circuito
	Ante el ingreso correcto de la contraseña se apaga
 * 	@return none */

static void LyA (void* h)
{
	while (1)
	{
        xSemaphoreTake(semb_AlyLu,(portMAX_DELAY));//Se activó la alarma
        //Chip_GPIO_SetPinState(LPC_GPIO, RELE1_PORT, RELE1_PIN, 1);// habilita el rele que esta NA y queda NC
        Chip_GPIO_SetPinOutHigh(LPC_GPIO,RELE1_PORT,RELE1_PIN);
        xSemaphoreTake(semb_Pass_OK, (portMAX_DELAY));//Se ingresa bien la contraseña
        //Chip_GPIO_SetPinState(LPC_GPIO, RELE1_PORT, RELE1_PIN, 0);// habilita el rele que esta NC y queda NA
        Chip_GPIO_SetPinOutLow(LPC_GPIO,RELE1_PORT,RELE1_PIN);
	}
}

/**	@brief :Permite determinar si el codigo ingresado es el mismo que esta guardado en memoria
 * 	@return none*/

//static void read_codigo (void* k)
//{
//
//	while (1)
//	{
//
//	//debe barrer el teclado con antirrebote y comparar con una contraseña guardada en algun lugar de memoria.  4 digitos
//		//debe contener eun if para que si el codigo ingresado es correcto, habilite la desconexion de la alarma con xSemaphoreGive(sem_AlyLu)
//
//	}
//}

/**	@brief: Arma el vector de datos de sensores para enviar al servidor. Esta tarea debe hacer el take del semaforo del log, sino nunca se ejecuta
 * 	@return none */

//static void armar_vector (void* k)
//{
//
//	while (1)
//	{
//	//***se va a implentar con cola que guarde nodos de la estructura***
//		if (xSemaphoreTake(sem_UART0,(portMAX_DELAY)) == pdTRUE)
//		{
//			// leer datos de sensores para armar el vector
//				vTaskDelay(50 / portTICK_RATE_MS);
//		}
//	}
//
//}

/**	@brief: Evita rebotes al apretar las teclas en el teclado
 * 	@return none */
void tecladoAntiRebote (void *pvParameters)
{
	keypad *kp4x4=(keypad *)pvParameters;
	portBASE_TYPE estadoMDE=ESTABLE;
	char teclaAnterior=getKey(kp4x4);
	char teclaActual;

	while(1)
	{
		teclaActual=getKey(kp4x4);
		switch(estadoMDE)
		{
		case ESTABLE:
			if(teclaAnterior!=teclaActual)
				estadoMDE=VALIDANDO;
			else
				vTaskDelay(BUTTON_SCAN_RATE_ms / portTICK_RATE_MS);
			break;

		case VALIDANDO:
			vTaskDelay(DEBOUNCE_TIME_ms / portTICK_RATE_MS);	//se puede cambiar por un vTaskDelayUntil
			if(teclaAnterior!=teclaActual)	//Si la tecla presionada se mantuvo distinta
			{
				//Chip_GPIO_SetPinToggle(LPC_GPIO,0,22);	//Toggleo el led (P0.22)
				if(teclaActual != NO_KEY)
				{	/* CÓDIGO SI SE PRESIONÓ UNA TECLA*/
					teclaAnterior=teclaActual;
					//printf("Tecla presionada: \t %c\n",(char)teclaActual);
					LCD_Print("Tecla presionada");
					//LCD_GoToxy(2, 2);

					//Meto la tecla en la cola
					xQueueSend(qPassword, &teclaActual, portMAX_DELAY);
				}
				else
				{	/* CÓDIGO SI SE SOLTÓ UNA TECLA*/
					//printf("Se solto la tecla\n");

				}
			}
			estadoMDE=ESTABLE;
			break;
		}
	}
}

/** @brief Esta función compara la contraseña ingresada con la guardada en la eeprom
 *	@return none
 */

void compara_password (void *a){
	int cLeidos=0, cIguales=0;
	char tecla;
	while(1)
	{
		xQueueReceive(qPassword, &tecla, portMAX_DELAY);
		if(tecla==password[cLeidos++])
			cIguales++;
		if(cLeidos==strlen(password))	//-1 por el 0x00 de fin de cadena
		{
			if(cLeidos==cIguales)//Si las contraseñas coinciden
				xSemaphoreGive(semb_Pass_OK);// Doy semáforo que indique que está ok
			cLeidos=0;
			cIguales=0;
		}
	}
}

//static void send_wifi (void *j){
//
//	/*Defino un vector "comando" donde armo las instrucciones a enviar para el modulo
//	 * ESP8266.
//	 * "data" es variable auxiliar, donde voy a alojar la longitud del mensaje (en caracteres)
//	 * que envio al modulo
//	*/
//	uint8_t comando[240], data;
//
//	while(1){
//		int i;
//		/* envio instruccion de conexion al server 192.168.1.101 por puerto 80, comunicacion tipo TCP
//		 es el primer paso para poder interactuar con el servidor*/
//
//		Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSTART=1,\"TCP\",\"192.168.1.101\",80\r\n", 39);
//		for(i=0; i<0xFFFF; i++);
//
//		/* dentro del script php en el server, estan definidos los parametros luz1, luz2
//		 * gas, vdl, puerta y temperatura. Al ejecutar el script (mediante el comando GET)
//		 * se guarda el valor que pasamos dentro del parametro que indicamos.
//		 * La forma es guardardatos.php?parametro=valor
//		 *
//		 */
//			if(strcmp((char *)dataArray[LUZ1],(char *)"ON")){  //debo saber si el estado de la luz1 es ON u OFF para armar el comando GET
//				strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?luz1=ON"); //si el estado es ON, indico que el parametro luz1 guarde el valor "ON"
//				strcat((uint8_t *)comando," HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");	//informacion necesaria para el comando GET, indica el host a donde estoy apuntando
//				data=63;	//longitud en caracteres del comando GET completo
//				Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,63\r\n", 17); //comando que indica que voy a enviar un mensaje al servidor, de 63 caracteres
//				for(i=0; i<0xFFFF; i++);
//			}
//			else{
//				strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?luz1=OFF"); //si el estado es OFF, indico que el parametro luz1 guarde el valor "OFF"
//				strcat((uint8_t *)comando," HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");	//informacion necesaria para el comando GET
//				data=64;	//longitud en caracteres del comando GET
//				Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,64\r\n", 17); //indico que voy a enviar al server un mensaje de 64 caracteres
//				for(i=0; i<0xFFFF; i++);
//			}
//
//
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)comando, data); //envio comando GET completo con el estado de la luz1 (sea ON u OFF)
//			for(i=0; i<0xFFFF; i++);
//
//			//el siguiente if cumple la misma funcion que el anterior, pero leyendo el estado de la luz2
//			if(strcmp((char *)dataArray[LUZ2],(char *)"ON")){
//				strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?luz2=ON");
//				strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//				data=63;
//				Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,63\r\n", 17);
//				for(i=0; i<0xFFFF; i++);
//			}
//			else{
//				strcpy((char *)comando, (char *)"GET /guardardatos.php?luz2=OFF ");
//				strcat((char *)comando, "HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//				data=64;
//				Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,64\r\n", 17);
//				for(i=0; i<0xFFFF; i++);
//			}
//
//
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)comando, data); //envio comando GET completo con el estado de la luz1 (sea ON u OFF)
//			for(i=0; i<0xFFFF; i++);
//
//			/* genero el comando GET para guardar el nivel de gas medido.
//			 */
//			strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?gas=");
//			strcat((uint8_t *)comando, (char *)dataArray[GAS]);
//			strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,62\r\n", 17);
//			for(i=0; i<0xFFFF; i++);
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)comando, 62); //envio comando GET completo con el nivel de gas medido
//			for(i=0; i<0xFFFF; i++);
//			/*
//			 * genero comando GET para guardar el nivel de la tension de linea
//			 */
//			strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?vdl=");
//			strcat((uint8_t *)comando, (char *)dataArray[VDL]);
//			strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,63\r\n", 17);
//			for(i=0; i<0xFFFF; i++);
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)comando, 63); //envio tension de linea
//			for(i=0; i<0xFFFF; i++);
//
//		/*
//		 * de la misma manera que checkeo el estado de las luces 1 y 2, verifico si la puerta esta
//		 * OPEN o CLOSE para generar primero el comando GET y luego enviarlo al server
//		 */
//		if(strcmp((char *)dataArray[PUERTA],(char *)"OPEN")){
//			strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?puerta=OPEN");
//			strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//			data=67;
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,67\r\n", 17);
//			for(i=0; i<0xFFFF; i++);
//			}
//			else{
//				strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?puerta=CLOSE");
//				strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//				data=68;
//				Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,68\r\n", 17);
//				for(i=0; i<0xFFFF; i++);
//			}
//		/* envio estado de puerta*/
//			Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)comando, data);
//			for(i=0; i<0xFFFF; i++);
//		/*
//		 * Genero comando GET para enviar la temperatura
//		 */
//		strcpy((uint8_t *)comando, (uint8_t *)"GET /guardardatos.php?temperatura=");
//		strcat((uint8_t *)comando, (char *)dataArray[TEMPERATURA]);
//		strcat((uint8_t *)comando,(uint8_t *)" HTTP/1.1\r\nHost: 192.168.1.101\r\n\r\n");
//
//		Chip_UART_SendBlocking(LPC_UART3, (uint8_t *)"AT+CIPSEND=1,72\r\n", 17); //envio temperatura
//		for(i=0; i<0xFFFF; i++);
//
//
//		vTaskDelay(2000 / portTICK_RATE_MS);
//
//	}
//}


/*==================[external functions definition]==========================*/

int main(void)
{
	initHardware();

/** Creacion de colas */
	qUART0 = xQueueCreate(32, sizeof(char));
	qUART1 = xQueueCreate(32, sizeof(char));
	qLCD   = xQueueCreate(32, sizeof(char));
	qCodigo = xQueueCreate(32, sizeof(char));
	qADC0 = xQueueCreate(32, sizeof(char));
	qADC1 = xQueueCreate(32, sizeof(char));
	qPassword = xQueueCreate(1, sizeof(char));


/* creacion de semaforos */
	//vSemaphoreCreateBinary(semb_Cod);
	//xSemaphoreTake(semb_Cod,portMAX_DELAY);
	vSemaphoreCreateBinary(semb_UART0);
	xSemaphoreTake(semb_UART0,portMAX_DELAY);
	vSemaphoreCreateBinary(semb_UART1);
	xSemaphoreTake(semb_UART1,portMAX_DELAY);
	vSemaphoreCreateBinary(semb_AlyLu);
	xSemaphoreTake(semb_AlyLu,portMAX_DELAY);
	vSemaphoreCreateBinary(semb_Linea);
	xSemaphoreTake(semb_Linea,portMAX_DELAY);
    vSemaphoreCreateBinary(semb_Pass_OK);
    //xSemaphoreTake(semb_Pass_OK,portMAX_DELAY);
//****************************************************************************
//* 	Configuración del teclado
//****************************************************************************/

/** Declaración del teclado matricial y su pinout.*/
	static keypad kp4x4;
	static char keys[4][4]={
			{'1','2','3','A'},
			{'4','5','6','B'},
			{'7','8','9','C'},
			{'*','0','#','D'}};
	static gpio pinoutKeypad[8];

	for(int i=0;i<8;i++)
	{
		pinoutKeypad[i].port=2;		/*el teclado matricial está en el puerto 2*/
		if(i<5) 					/*los pines van del 4 al 8 y del 10 al 12*/
			pinoutKeypad[i].pin=4+i;
		else
			pinoutKeypad[i].pin=4+i+1;
	}
/** keypad setup */
	keypadInit(&kp4x4, 4, 4, &pinoutKeypad[0], &pinoutKeypad[4], &keys[0][0]);

/****************************************************************************
* 	Fin configuración del teclado
****************************************************************************/

/** Parametros: nombre de la tarea, string que muestra lo que se esta haciendo,el tamaño de pila, poner 512bytes o 1kb de ser posible para que no de error
 el parametro 0 es para instanciar las tareas, parametrizacion de tareas.Sirve para dos tareas que tienen codigo similar
El mensaje Hard Fault es porque se queda sin pila alguna tarea. La prioridad esta creada por macro en freertos. El ultimo parmetro que aca se ve como 0,
 puede aparecer como NULL pero en realidad es &taskID */

// se pueden leer varios pines en una sola tarea + delay
//	xTaskCreate(read_ED0, (const signed char * const)"read_ED0", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	xTaskCreate(read_sensores, (const signed char * const)"read_sensores", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

//	xTaskCreate(read_ED1, (const signed char * const)"read_ED1", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

//	xTaskCreate(read_ED2, (const signed char * const)"read_ED2", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	//xTaskCreate(read_EA1, (const signed char * const)"read_EA1", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	//xTaskCreate(read_EA2, (const signed char * const)"read_EA2", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	//xTaskCreate(v_linea, (const signed char * const)"corte_linea", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	xTaskCreate(send_SMS, (const signed char * const)"send_SMS", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);

	xTaskCreate(LyA, (const signed char * const)"Luz_Sirena", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);

    xTaskCreate(compara_password, (const signed char * const)"Compara password", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);

	//xTaskCreate(send_wifi, (const char *)"send_wifi", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

//	xTaskCreate(armar_vector, (const signed char * const)"armar vector", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

//	xTaskCreate(read_codigo, (const signed char * const)"read_codigo", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);



	xTaskCreate(	tecladoAntiRebote,
					(const signed char * const)"Teclado AntiRebote",
					configMINIMAL_STACK_SIZE*4,
					(void *)&kp4x4,
					tskIDLE_PRIORITY+2,
					NULL );

	xTaskCreate(	debug,
						(const signed char * const)"debug task",
						configMINIMAL_STACK_SIZE*2,
						0,
						tskIDLE_PRIORITY+2,
						NULL );

	//una vez que se entra aca ya no se deberia salir
	vTaskStartScheduler();
	// si se vuelve al main en este punto es porque algo fallo
	while (1) {

				}


}


/** @} doxygen end group definition */

/*==================[end of file]============================================*/
