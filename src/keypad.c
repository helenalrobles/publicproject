/* Copyright 2016, S. Schachtner
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @brief		Teclado matricial de 4x4 teclas con antirebote usando FreeRTOS.
 *  @details	Captura las teclas presionadas y las envía por SEMIHOSTING
 * 				o puerto serie según la configuración.
 */

/*==================[inclusions]=============================================*/

#include "keypad.h"

//#include "board.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

/**	@brief	Pone en alto todas las columnas para hacer el barrido del teclado
 *	@params	[out]	kp		Puntero a la estructura que contiene los datos del teclado.
 */
static void clearCols(const keypad * const kp)
{
	for(int i=0; i < kp->nCols ; i++)
		Chip_GPIO_SetPinState(LPC_GPIO,kp->cols[i].port,kp->cols[i].pin,true);
}

/**	@brief	Configura el teclado matricial
 *	@params	[out]	kp		Puntero a la estructura donde se van a inicializar los datos del teclado
 *	@params [in]	nRows	Cantidad de filas del teclado.
 *	@params [in]	nCols	Cantidad de columnas del teclado.
 *	@params [in]	rows	Puntero a array de gpio que contiene el puerto y pin de cada fila.
 *	@params [in]	rows	Puntero a array de gpio que contiene el puerto y pin de cada columna.
 *	@params [in]	rows	Puntero a array que contiene las letras del teclado.
 */
void keypadInit(keypad * const kp,	uint32_t nRows,	uint32_t nCols,	gpio *rows,	gpio *cols,	char *keys)
{
	//TeclX1: P2.4;TeclX2: P2.5;TeclX3: P2.6;TeclX4: P2.7;TeclY1: P2.8;TeclY2: P2.10;TeclY3: P2.11;TeclY4: P2.12;
	kp->nCols=nCols;
	kp->nRows=nRows;
	kp->keys=keys;
	kp->cols=cols;
	kp->rows=rows;

	/* GPIO setup */

	//configuro las filas como entradas
	for(int row=0; row < kp->nRows ; row++)
	{
		Chip_IOCON_PinMux(LPC_IOCON,
				kp->rows[row].port,
				kp->rows[row].pin,
				IOCON_MODE_PULLUP,
				IOCON_FUNC0);
		Chip_GPIO_SetPinDIRInput(LPC_GPIO,kp->rows[row].port,kp->rows[row].pin);
	}

	//configuro las columnas como salidas
	for(int col=0; col < kp->nCols ; col++)
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,kp->cols[col].port,kp->cols[col].pin);

	//clearCols(kp);	//Setea todo en alto o bajo (VER segun pull-up o pull-down)
}

/**	@brief	Configura el teclado matricial
 *	@params	[out]	kp		Puntero a la estructura donde se van a inicializar los datos del teclado
 *	@return					Letra presionada o NO_KEY en caso de no presionarse ninguna letra.
 */
char getKey(const keypad * const kp)
{
	for(int col=0; col < kp->nCols ; col++)
	{
		clearCols(kp);
		Chip_GPIO_SetPinState(LPC_GPIO,kp->cols[col].port,kp->cols[col].pin,false);

		for(int row=0 ; row < kp->nRows ; row++)
			if(Chip_GPIO_GetPinState(LPC_GPIO,kp->rows[row].port,kp->rows[row].pin)==false)
				return kp->keys[(row*kp->nRows)+col];

		//Chip_GPIO_SetPinState(LPC_GPIO,kp->cols[col].port,kp->cols[col].pin,true);
	}
	return NO_KEY;
}
/*==================[end of file]============================================*/
